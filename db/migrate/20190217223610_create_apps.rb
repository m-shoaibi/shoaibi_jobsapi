class CreateApps < ActiveRecord::Migration[5.2]
  def change
    create_table :apps do |t|
      t.string :Job
      t.boolean :Seen
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
